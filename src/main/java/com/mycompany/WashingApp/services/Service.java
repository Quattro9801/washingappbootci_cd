package com.mycompany.WashingApp.services;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.services.carService.CarServiceImpl;
import com.mycompany.WashingApp.services.carWashService.CarWashService;
import com.mycompany.WashingApp.services.locationService.LocationService;
import com.mycompany.WashingApp.services.modelCarService.ModelCarService;
import com.mycompany.WashingApp.services.orderService.OrderService;
import com.mycompany.WashingApp.services.reviewService.ReviewService;
import com.mycompany.WashingApp.services.typeCarService.TypeCarService;
import com.mycompany.WashingApp.services.userService.UserService;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class Service {
    @Autowired
    private UserService userService;
    @Autowired
    com.mycompany.WashingApp.services.orderService.OrderService orderService;
    @Autowired
    CarServiceImpl carService;

    @Autowired
    CarWashService carWashService;

    @Autowired
    LocationService locationService;

    @Autowired
    ModelCarService modelCarService;

    @Autowired
    TypeCarService typeCarService;

    @Autowired
    com.mycompany.WashingApp.services.Service_For_Wash.Service service;

    @Autowired
    ReviewService reviewService;

    public Service() {
    }

    public UserService getUserService() {
        return userService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public CarServiceImpl getCarService() {
        return carService;
    }

    public CarWashService getCarWashService() {
        return carWashService;
    }

    public LocationService getLocationService() {
        return locationService;
    }

    public ModelCarService getModelCarService() {
        return modelCarService;
    }

    public TypeCarService getTypeCarService() {
        return typeCarService;
    }

    public com.mycompany.WashingApp.services.Service_For_Wash.Service getService() {
        return service;
    }

    public ReviewService getReviewService() {
        return reviewService;
    }
}
