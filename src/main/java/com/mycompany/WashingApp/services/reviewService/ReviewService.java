package com.mycompany.WashingApp.services.reviewService;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Review;

import java.util.List;
import java.util.Optional;

public interface ReviewService {
    Review createReview(CarWash carWash, Review review);
    Optional<Review> getReviewById(Integer id);
    void deleteReview(Integer id);
    List<Review> getAllReviews();
}
