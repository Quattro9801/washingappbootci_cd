package com.mycompany.WashingApp.services.reviewService;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Review;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import com.mycompany.WashingApp.jpaExample.ReviewRepo;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReviewsServiceImpl implements ReviewService {
    @Autowired
    ReviewRepo reviewRepo;
    @Override
    public Review createReview(CarWash carWash, Review review) {
        return reviewRepo.createReview(carWash, review);
    }

    @Override
    public Optional<Review> getReviewById(Integer id) {
        Optional<Review> review = reviewRepo.findById(id);
        if (!review.isPresent()) {
            throw new NoSuchElementException("There is no review with id " + id + " in Database");
        }
        return review;
    }

    @Override
    public void deleteReview(Integer id) {
        reviewRepo.deleteById(id);

    }

    @Override
    public List<Review> getAllReviews() {
        return reviewRepo.findAll();
    }
}
