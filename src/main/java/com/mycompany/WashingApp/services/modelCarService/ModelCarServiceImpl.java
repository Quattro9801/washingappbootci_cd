package com.mycompany.WashingApp.services.modelCarService;

import com.mycompany.WashingApp.DAO.ModelCarDao;
import com.mycompany.WashingApp.entity.ModelCar;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModelCarServiceImpl implements ModelCarService{
    @Autowired
    ModelCarDao modelCarDao;
    @Override
    public ModelCar getModelCarById(int id) {
        ModelCar modelCar = modelCarDao.getModelCarById(id);
        if (modelCar == null) {
            throw new NoSuchElementException("There is no modelCar with id = " + id);
        }
        return modelCar;
    }
}
