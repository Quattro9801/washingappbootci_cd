package com.mycompany.WashingApp.services.Service_For_Wash;

import com.mycompany.WashingApp.DAO.ServiceDao;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
@org.springframework.stereotype.Service
public class ServiceImpl implements Service {

    @Autowired
    ServiceDao serviceDao;
    @Override
    public com.mycompany.WashingApp.entity.Service addService(com.mycompany.WashingApp.entity.Service service) {
        return serviceDao.addService(service);
    }

    @Override
    public List<com.mycompany.WashingApp.entity.Service> getAllServices() {
        return serviceDao.getAllServices();
    }

    @Override
    public com.mycompany.WashingApp.entity.Service getServiceById(Integer id) {
        com.mycompany.WashingApp.entity.Service service = serviceDao.getServiceById(id);
        if (service == null) {
            throw new NoSuchElementException("There is no service with id " + id + " in Database");
        }
        return service;
    }

    @Override
    public com.mycompany.WashingApp.entity.Service updateServiceById(Integer id, com.mycompany.WashingApp.entity.Service service) {
        getServiceById(id);
        return serviceDao.updateService(id, service);
    }

    @Override
    public void deleteServiceById(Integer id) {

    }


}
