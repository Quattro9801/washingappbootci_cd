package com.mycompany.WashingApp.services.carWashService;

import com.mycompany.WashingApp.DAO.CarWashDao;
import com.mycompany.WashingApp.DAO.LocationDao;
import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Location;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import com.mycompany.WashingApp.services.locationService.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarWashServiceImpl implements CarWashService{
    @Autowired
    CarWashDao carWashDao;
    @Override
    public CarWash getCarWashById(Integer id) {
        CarWash carWash = carWashDao.getCarWashById(id);
        return carWash;
    }

    @Override
    public CarWash createCarWash(CarWash carWash, Location location) {
        return carWashDao.createCarWash(carWash, location);
    }

    @Override
    public List<CarWash> getAllCarWashes() {
        return carWashDao.getAllCarWashes();
    }

    @Override
    public CarWash updateCarWashById(Integer id, CarWash carWash) {
        //locationService.getLocationById(carWash.getLocation().getLocationId());
       /* if (id != carWash.getLocation().getLocationId()) {
            throw new NoSuchElementException("Id must be equal");// to do другой экзепшен
        }*/

        return carWashDao.updateCarWash(id, carWash);
    }
}
