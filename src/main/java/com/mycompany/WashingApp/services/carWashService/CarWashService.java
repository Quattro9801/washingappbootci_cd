package com.mycompany.WashingApp.services.carWashService;

import com.mycompany.WashingApp.entity.CarWash;
import com.mycompany.WashingApp.entity.Location;

import java.util.List;

public interface CarWashService {
    CarWash getCarWashById(Integer id);
    CarWash createCarWash(CarWash carWash, Location location);
    List<CarWash> getAllCarWashes();
    CarWash updateCarWashById(Integer id, CarWash carWash);
}
