package com.mycompany.WashingApp.services.carService;

import com.mycompany.WashingApp.DAO.CarDao;
import com.mycompany.WashingApp.entity.Car;
import com.mycompany.WashingApp.entity.ModelCar;
import com.mycompany.WashingApp.entity.TypeCar;
import com.mycompany.WashingApp.entity.User;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarServiceImpl implements CarService {
    @Autowired
    CarDao carDao;
    @Override
    public Car createCar(Car car,User user, ModelCar modelCar, TypeCar typeCar) {
        return carDao.createCar(car,user, modelCar, typeCar);
    }

    @Override
    public Car getCarById(Integer id) {
        Car car = carDao.getCarById(id);
        if (car == null) {
            throw new NoSuchElementException("There is no car with id " + id + " in Database");
        }
        return car;
    }

    @Override
    public List<Car> getAllCarsByUser(Integer id) {
        return carDao.getAllCarsByUser(id);
    }
}
