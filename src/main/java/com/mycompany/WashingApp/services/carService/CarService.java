package com.mycompany.WashingApp.services.carService;

import com.mycompany.WashingApp.entity.Car;
import com.mycompany.WashingApp.entity.ModelCar;
import com.mycompany.WashingApp.entity.TypeCar;
import com.mycompany.WashingApp.entity.User;

import java.util.List;

public interface CarService {
    Car createCar(Car car, User user, ModelCar modelCar, TypeCar typeCar);
    Car getCarById(Integer id);
    List<Car> getAllCarsByUser(Integer id);
}
