package com.mycompany.WashingApp.services.orderService;

import com.mycompany.WashingApp.DAO.OrderDao;
import com.mycompany.WashingApp.entity.*;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import com.mycompany.WashingApp.model.ServiceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderDao orderDao;
    @Override
    public Order createOrder(User user, CarWash carWash, Car car, LocalDateTime date) {
        return orderDao.createOrder(user,carWash, car, date);

    }

    @Override
    public Order getOrderById(Integer id) {
        Order order = orderDao.getOrderById(id);
        if (order == null) {
            throw new NoSuchElementException("Order with id " + id + " " + "not found");
        }
        return order;
    }

    @Override
    public List<Order> getOrdersByUserId(Integer id) {
        return orderDao.getOrdersByUserId(id);
    }

    @Override
    public Order addOrderItemToSo(Order order, ServiceModel serviceModel) {
        return orderDao.addOrderItemToSalesOrder(order, serviceModel);
    }

    @Override
    public void deleteOrderItem(OrderItem orderItem, Integer orderId) {
        orderDao.deleteOrderItem(orderItem, orderId);
    }

/*    @Override
    public void deleteOrderItem(int id) {
        Order order = orderDao.getOrderById(id);
        if (order == null) {
            throw new NoSuchUserException("Order with id " + id + " " + "not found");
        }
        return orderDao.getOrderById(id);
    }*/

    @Override
    public OrderItem getOrderItemById(Integer id) {
        OrderItem orderItem = orderDao.getOrderItemById(id);
        if (orderItem == null) {
            throw new NoSuchElementException("OrderItem with id " + id + " " + "not found in database");
        }
        return orderItem;
    }

    @Override
    public Order changeOrderStatus(Integer orderId, StatusOrder status) {
        return orderDao.changeOrderStatus(getOrderById(orderId), status);
    }

    @Override
    public StatusOrder getStatusOrderById(Integer id) {
        StatusOrder statusOrder = orderDao.getStatusOrderById(id);
        if (statusOrder == null) {
            throw new NoSuchElementException("Status order with id " + id + " " + "not found in database");
        }
        return statusOrder;
    }

    @Override
    public List<StatusOrder> getAllStatuses() {
        return orderDao.getAllStatuses();
    }

    @Override
    public StatusOrder createStatusOrder(StatusOrder statusOrder) {
        return orderDao.createStatusOrder(statusOrder);
    }


}
