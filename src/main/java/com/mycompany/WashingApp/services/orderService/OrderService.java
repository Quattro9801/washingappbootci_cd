package com.mycompany.WashingApp.services.orderService;

import com.mycompany.WashingApp.entity.*;
import com.mycompany.WashingApp.model.OrderModel;
import com.mycompany.WashingApp.model.ServiceModel;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface OrderService {
    Order createOrder(User user, CarWash carWash, Car car, LocalDateTime date);
    Order getOrderById(Integer id);
    List<Order> getOrdersByUserId(Integer id);
    Order addOrderItemToSo(Order order, ServiceModel service);
    void deleteOrderItem(OrderItem orderItem, Integer id);
    OrderItem getOrderItemById(Integer id);
    Order changeOrderStatus(Integer orderId, StatusOrder status);
    StatusOrder getStatusOrderById(Integer id);
    List<StatusOrder> getAllStatuses();
    StatusOrder createStatusOrder(StatusOrder statusOrder);

    //deleteStatusOrder();

}
