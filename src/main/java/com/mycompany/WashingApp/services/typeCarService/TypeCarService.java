package com.mycompany.WashingApp.services.typeCarService;

import com.mycompany.WashingApp.entity.TypeCar;

public interface TypeCarService {
    TypeCar getTypeCarById(Integer id);
}
