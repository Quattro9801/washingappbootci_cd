package com.mycompany.WashingApp.services.typeCarService;

import com.mycompany.WashingApp.DAO.TypeCarDao;
import com.mycompany.WashingApp.entity.TypeCar;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TypeCarServiceImpl implements TypeCarService {
    @Autowired
    TypeCarDao typeCarDao;
    @Override
    public TypeCar getTypeCarById(Integer id) {
        TypeCar typeCar = typeCarDao.getTypeCarById(id);
        if (typeCar == null) {
            throw new NoSuchElementException("There is no modelCar with id = " + id);
        }
        return typeCar;
    }

}
