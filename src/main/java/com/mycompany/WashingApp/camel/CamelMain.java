package com.mycompany.WashingApp.camel;

import com.mycompany.WashingApp.DAO.UserDao;
import com.mycompany.WashingApp.implimentation.CheckDbDAO;
import com.mycompany.WashingApp.implimentation.UserDAOImpl;
import com.mycompany.WashingApp.services.userService.UserServiceImplService;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.stereotype.Component;

import java.net.ConnectException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class CamelMain {

    public void run() throws Exception {
        CamelContext camelContext = new DefaultCamelContext();
        camelContext.getPropertiesComponent().setLocation("classpath:application.properties");
        camelContext.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("timer:foo")
                        .doTry()
                        .bean(CamelMain.class, "getList")
                        .bean(CamelMain.class, "toA")
                        .doCatch(ConnectException.class)
                        //.end()
                        .stop()
                        .end()
                        .bean(CamelMain.class, "toB");

                      /*  .doCatch(Exception.class)
                        .to("log:?showBody=true&showHeaders=true")
                        .end()
                        .bean(CamelEx.class, "printSuccess");*/
            }
        });


        camelContext.start();
        Thread.sleep(4_000);
        camelContext.stop();
        camelContext.close();
    }

    public List<Integer> getList() throws SQLException, ConnectException {
        List<Integer> list = new ArrayList<>();
        for (int i=0;i<1000000;i++) {
            list.add(new Random().nextInt());
            list.add(1);
        }

       for (Integer i:list) {
           if (i == 1)
            throw new ConnectException();
        }
        return list;
    }

    public void toA () {
        System.out.println("This is a");
    }

    public void toB () {
        System.out.println("This is B");
    }
    public void wireTap () throws ConnectException {
        List<Integer> list = new ArrayList<>();
        for (int i=0;i<1000000;i++) {
            list.add(new Random().nextInt());
            list.add(1);
        }

        for (Integer i:list) {
            if (i == 1)
                throw new ConnectException();
        }
        System.out.println("Inside wiretap");
    }

    public void getEnd() throws SQLException, ConnectException {
        getList();

        System.out.println("This is end");
    }
}


