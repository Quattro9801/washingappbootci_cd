package com.mycompany.WashingApp.camel;

import com.mycompany.WashingApp.controllers.UserController;
import com.mycompany.WashingApp.entity.Car;
import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.net.ConnectException;
@Component
public class ProdandConsumer {
    @Autowired
    UserController userController;

    public void run() throws Exception {
    CamelContext camelContext = new DefaultCamelContext();
        camelContext.getPropertiesComponent().setLocation("classpath:application.properties");
        camelContext.addRoutes(new RouteBuilder() {
        @Override
        public void configure() throws Exception {
            from("direct:start123")
                    .to("seda:end")
               /*     .setHeader(Exchange.HTTP_METHOD, simple("GET"))
                    .to("http://localhost:8080/api/v1/reviewManagement/reviews")

                    .process(new Processor() {
                        @Override
                        public void process(Exchange exchange) throws Exception {
                            String output = exchange.getIn().getBody(String.class);
                            System.out.println(output);
                        }
                    })*/
                    .log("Main route execution for body ")
                    .wireTap("direct:wireTap")
                    .bean(CamelMain.class, "getEnd");


                from ("direct:wireTap")
                        .log("This is WireTap")
                        .bean(CamelMain.class, "wireTap");

                      /*  .doCatch(Exception.class)
                        .to("log:?showBody=true&showHeaders=true")
                        .end()
                        .bean(CamelEx.class, "printSuccess");*/
        }
    });

        camelContext.start();

        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        producerTemplate.sendBody("direct:start123", userController.getUsers());
        ConsumerTemplate consumerTemplate = camelContext.createConsumerTemplate();

        String message = consumerTemplate.receiveBody("seda:end", String.class);
        //System.out.println("Message from consumer = " +message);
        //Thread.sleep(10_000);
        camelContext.stop();
        camelContext.close();
}
}
