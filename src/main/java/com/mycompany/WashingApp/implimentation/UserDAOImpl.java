package com.mycompany.WashingApp.implimentation;

import com.mycompany.WashingApp.DAO.UserDao;
import com.mycompany.WashingApp.entity.Order;
import com.mycompany.WashingApp.entity.User;
import com.mycompany.WashingApp.entity.UserWithEmail;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Map;


@Repository
@Transactional
public class UserDAOImpl implements UserDao {
    @Autowired
    EntityManager transactionManager;

    @Override
    public User addUser(User user) {
        transactionManager.merge(user);
        System.out.println("---------------------------------");
        System.out.println("User was added");
        System.out.println("---------------------------------");
        return user;
    }

    @Override
    public void removeUserById(Integer id) {
        User user = getUserTest(id);
        if (user == null) {
            throw new NoSuchElementException("There is no user with id = " + id);
        }
        //Ищем все ордера по юзеру
        Query findOrdersById = transactionManager.createNativeQuery("SELECT order_id from orders o where o.user_id = ?");
        findOrdersById.setParameter(1, id);
        List<Integer> orderIds = findOrdersById.getResultList();

        //Удаляем элементы по orderIds
        Query findOrderItemsByOrderIds = transactionManager.createNativeQuery("DELETE from orderItems oi where oi.order_id IN (:orderIds)");
        findOrderItemsByOrderIds.setParameter("orderIds", orderIds);
        findOrderItemsByOrderIds.executeUpdate();

        //Удаляем ордера
        Query fromOrder = transactionManager.createNativeQuery("DELETE FROM orders o WHERE o.user_id = ?");
        fromOrder.setParameter(1, id);
        fromOrder.executeUpdate();

        //Удаляем машины по юзеру
        Query fromCar = transactionManager.createNativeQuery("DELETE FROM cars ca WHERE ca.user_id = ?");
        fromCar.setParameter(1, id);
        fromCar.executeUpdate();

        transactionManager.remove(user);
        System.out.println("Success");
    }

    @Override
    public User getUserById(Integer id) {
        User user = null;
      /*  Session session = transactionManager.getSessionFactory().getCurrentSession();
        User user = (User) session.get(User.class, id);
        System.out.println("FindUserByID " + user);*/
            jakarta.persistence.Query userQuery = transactionManager.createQuery("SELECT new com.mycompany.WashingApp.entity.User(u.id, u.name,u.surname,u.dateOfBirthday,u.email,u.phoneNumber) FROM User u where u.id=:id").setParameter("id", id);
            //Query<User> userQuery = session.createQuery("SELECT new com.mycompany.hibernate.washing.entity.User(u.id,u.name,u.surname,u.dateOfBirthday,u.email,u.phoneNumber) FROM User u");
             user = (User) userQuery.getResultList().stream().findFirst().orElse(null);

     /*   Session session = transactionManager.getSessionFactory().getCurrentSession();
        org.hibernate.query.Query<UserModel> userQuery = session.createQuery("SELECT new com.mycompany.hibernate.washing.model.UserModel (u.id ,u.name, u.surname, u.dateOfBirthday,u.email,u.phoneNumber) FROM User u WHERE  u.id=:id").setParameter("id",id);*/
        return user;
    }

    @Override
    public User updateUserById(Integer id, User user) {
        //User currentUser = session.get(User.class, id);
        //Query userQuery = transactionManager.createQuery("SELECT new com.mycompany.WashingApp.entity.User(u.id, u.name,u.surname,u.dateOfBirthday,u.email,u.phoneNumber) FROM User u where u.id=:id").setParameter("id", id);
        //Query<User> userQuery = session.createQuery("SELECT new com.mycompany.hibernate.washing.entity.User(u.id,u.name,u.surname,u.dateOfBirthday,u.email,u.phoneNumber) FROM User u");
        //User currentUser = (User) userQuery.getResultList().stream().findFirst().orElse(null);
        User currentUser = transactionManager.find(User.class, id);
        if (currentUser == null) {
            throw new NoSuchElementException("There is no user with id = " + id);
        }
        currentUser.setName(user.getName());
        currentUser.setSurname(user.getSurname());
        currentUser.setDateOfBirthday(user.getDateOfBirthday());
        currentUser.setEmail(user.getEmail());
        currentUser.setPhoneNumber(user.getPhoneNumber());
        transactionManager.merge(currentUser);
        return currentUser;
    }

    @Override
    public User updateEmail(Integer id, UserWithEmail user) {
        User currentUser = transactionManager.find(User.class, id);
        if (currentUser == null) {
            throw new NoSuchElementException("There is no user with id = " + id);
        }
        currentUser.setEmail(user.getEmail());
        transactionManager.merge(currentUser);
        return currentUser;
    }

    @Override
    public User getUserTest(Integer id) {
        User user = (User) transactionManager.find(User.class, id);
        //user.getOrders();
        return user;
    }

    @Override
    public List<User> getAllUsersTest() {
        jakarta.persistence.Query userQuery = transactionManager.createQuery("FROM User", User.class);
        return userQuery.getResultList();
    }

    @Override
    public List<User> getAllUsers() {
        List<User> userList = null;
        jakarta.persistence.Query userQuery = transactionManager.createQuery("SELECT new com.mycompany.WashingApp.entity.User(u.id, u.name,u.surname,u.dateOfBirthday,u.email,u.phoneNumber) FROM User u");
        userList = userQuery.getResultList();
        return userList;
    }

  /*  @Override
    public List<Order> getAllOrdersByUser(BigInteger userId) {
        List<Order> orderList;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            User user = (User) session.get(User.class, userId);
            orderList = user.getOrders();
            session.getTransaction().commit();
            System.out.println("All orders was gotten By user" + user);

        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return orderList;
    }*/
}
