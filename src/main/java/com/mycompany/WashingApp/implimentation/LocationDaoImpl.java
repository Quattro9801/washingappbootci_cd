package com.mycompany.WashingApp.implimentation;

import com.mycompany.WashingApp.DAO.LocationDao;
import com.mycompany.WashingApp.entity.Location;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
@Transactional
public class LocationDaoImpl implements LocationDao {
    @Autowired
    EntityManager transactionManager;
    @Override
    public Location createLocation(Location location) {
        return transactionManager.merge(location);
    }

    @Override
    public List<Location> getAllLocations() {
        List<Location> locations = null;
        Query query = transactionManager.createQuery("FROM Location", Location.class);
        return query.getResultList();
    }

    @Override
    public Location getLocationById(Integer id) {
         return transactionManager.find(Location.class, id);
    }

    @Override
    public Location updateLocationById(Location location, Location currentLoc) {
        currentLoc.setAddressName(location.getAddressName());
        transactionManager.merge(currentLoc);
        return currentLoc;
    }


}
