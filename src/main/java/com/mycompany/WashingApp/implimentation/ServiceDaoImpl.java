package com.mycompany.WashingApp.implimentation;

import com.mycompany.WashingApp.DAO.ServiceDao;
import com.mycompany.WashingApp.entity.Service;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
@Transactional
public class ServiceDaoImpl implements ServiceDao {
    @Autowired
    EntityManager transactionManager;
    @Override
    public Service addService(Service service) {
        transactionManager.merge(service);
        return service;
    }

    @Override
    public void deleteServiceById(Integer id) {
       Service service = getServiceById(id);
        transactionManager.remove(service);
    }

    @Override
    public Service updateService(Integer id, Service service) {
       Service currentService =  getServiceById(id);
       currentService.setServiceName(service.getServiceName());
        currentService.setPrice(service.getPrice());
       currentService.setDescription(service.getDescription());
       transactionManager.merge(currentService);
       return currentService;
    }

    @Override
    public List<Service> getAllServices() {
        List<Service> allServices = transactionManager.createQuery("FROM Service", Service.class).getResultList();
        return allServices;
    }

    @Override
    public Service getServiceById(Integer id) {
        Service service = transactionManager.find(Service.class, id);
        return service;
    }
}
