package com.mycompany.WashingApp.implimentation;

import com.mycompany.WashingApp.DAO.CarDao;
import com.mycompany.WashingApp.entity.*;
import com.mycompany.WashingApp.exceptions.NoSuchElementException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class CarDaoImpl implements CarDao {
    @Autowired
    EntityManager transactionManager;
    @Override
    public Car createCar(Car car, User user, ModelCar modelCar, TypeCar typeCar) {
        car.setModelCar(modelCar);
        car.setUser(user);
        car.setTypeCar(typeCar);
        transactionManager.merge(car);
        System.out.println("---------------------------------");
        System.out.println("Car was created)");
        System.out.println("---------------------------------");
        return car;

    }

    @Override
    public void deleteCar(Integer id) {

    }

    @Override
    public Car updateCar(Integer id, Car car) {
        return null;
    }

    @Override
    public Car getCarById(Integer id) {
        Car car = (Car) transactionManager.find(Car.class, id);
        return car;
    }

    public List<Car> getAllCarsByUser(Integer id) {
        List<Car> list = null;
        Query userQuery = transactionManager.createQuery(" FROM Car c where c.user.id  = :id", Car.class).setParameter("id", id);
        list = userQuery.getResultList();
        System.out.println();
        return list;
    }

  /*  @Autowired
    FactoryBuilder factoryBuilder;

    public CarDaoImpl() {
    }

    @Override
    public void createCar(Car car) {
        try {
            Session session =factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(car);
            System.out.println("Car was created" + car);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
    }

    @Override
    public void deleteCar(int id) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Car car = (Car) session.get(Car.class, id);
            session.delete(car);
            System.out.println("Car was deleted " + car);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }

    }

    @Override
    public Car updateCar(int id, Car c) {
        Car car = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            car = (Car) session.get(Car.class, id);
            car.setCarName(c.getCarName());
            session.save(car);
            System.out.println("Car " + car);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return car;

    }

    @Override
    public Car getCarById(int id) {
        Car car = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            car = (Car) session.get(Car.class, id);
            System.out.println("Car " + car);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();
        }
     return car;
    }*/
}
