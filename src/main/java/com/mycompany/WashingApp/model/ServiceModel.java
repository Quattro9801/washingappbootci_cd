package com.mycompany.WashingApp.model;

import lombok.Data;

@Data
public class ServiceModel {

    private int serviceId;

    public ServiceModel() {
    }

    public ServiceModel(int serviceId) {
        this.serviceId = serviceId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }
}
