package com.mycompany.WashingApp.model;

public class StatusOrderModel {
    private int statusOrderId;

    public StatusOrderModel() {
    }

    public StatusOrderModel(int statusOrderId) {
        this.statusOrderId = statusOrderId;
    }

    public int getStatusOrderId() {
        return statusOrderId;
    }

    public void setStatusOrderId(int statusOrderId) {
        this.statusOrderId = statusOrderId;
    }
}
