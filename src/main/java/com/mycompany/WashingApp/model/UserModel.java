package com.mycompany.WashingApp.model;

import lombok.Data;

import java.util.Date;

@Data
public class UserModel {
    private int id;
    private String name;
    private String surname;
    private Date dateOfBirthday;
    private String email;
    private String phoneNumber;

    public UserModel(int id, String name, String surname, Date dateOfBirthday, String email, String phoneNumber) {
        id = id;
        this.name = name;
        this.surname = surname;
        this.dateOfBirthday = dateOfBirthday;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }


}
