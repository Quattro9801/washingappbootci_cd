package com.mycompany.WashingApp.DAO;

import com.mycompany.WashingApp.entity.ModelCar;

import java.util.List;

public interface ModelCarDao {
    void addModelCar(ModelCar modelCar);
    void deleteModelCar(Integer id);
    List<ModelCar> getAllModelCars();
    ModelCar getModelCarById(Integer id);
    ModelCar updateModelCar(Integer id, ModelCar updatedModelCar);
}
