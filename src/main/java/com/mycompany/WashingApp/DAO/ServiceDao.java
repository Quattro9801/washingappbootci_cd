package com.mycompany.WashingApp.DAO;

import com.mycompany.WashingApp.entity.Service;

import java.util.List;

public interface ServiceDao {
    Service addService(Service service);
    void deleteServiceById(Integer id);
    Service updateService(Integer id, Service service);
    List<Service> getAllServices();
    Service getServiceById(Integer id);
}
