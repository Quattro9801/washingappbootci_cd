package com.mycompany.WashingApp.DAO;

import com.mycompany.WashingApp.entity.Car;
import com.mycompany.WashingApp.entity.ModelCar;
import com.mycompany.WashingApp.entity.TypeCar;
import com.mycompany.WashingApp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarDao  {
    Car createCar(Car car, User user, ModelCar modelCar, TypeCar typeCar);
    void deleteCar (Integer id);
    Car updateCar (Integer id, Car car);
    Car getCarById(Integer id);
    List<Car> getAllCarsByUser(Integer id);
}
