package com.mycompany.WashingApp.DAO;

import com.mycompany.WashingApp.entity.User;
import com.mycompany.WashingApp.entity.UserWithEmail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDao {
    User addUser(User user);
    void removeUserById (Integer id);
    User getUserById(Integer id);
    User updateUserById(Integer id, User user);
    List<User> getAllUsers();
    User updateEmail(Integer id, UserWithEmail user);
    User getUserTest(Integer id);
    List<User> getAllUsersTest();
}
