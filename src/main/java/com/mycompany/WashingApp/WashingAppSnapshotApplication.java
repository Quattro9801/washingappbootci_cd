package com.mycompany.WashingApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class WashingAppSnapshotApplication {

	public static void main(String[] args) {
		SpringApplication.run(WashingAppSnapshotApplication.class, args);
	}

}
