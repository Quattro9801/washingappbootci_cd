package com.mycompany.WashingApp.entity;

import lombok.NoArgsConstructor;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@NoArgsConstructor
@Table(name = "orderitems")
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_item_id")
    private Integer orderItemId;
    @ManyToOne(cascade = {CascadeType.ALL},fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id")//аннот
    private Order order;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    @JoinColumn(name = "service_id")//аннотация говорит где искать связь между таблицами
    private Service service;

    public OrderItem(int orderItemId) {
        this.orderItemId = orderItemId;
    }


    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

  /*  @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        OrderItem orderItem = (OrderItem) o;
        return this.getService().getServiceName().equals( orderItem.getService().getServiceName());
    }*/

  /*  @Override
    public int hashCode() {
        int result = 17;
        if (getService().getServiceName() != null) {
            result = 31 * result + getService().getServiceName().hashCode();
        }
        return result;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        if (getService() == null || orderItem.getService() == null) {
            return Objects.equals(service, orderItem.getService());
        }
        return getService().getServiceName().equals(orderItem.getService().getServiceName());
    }

    @Override
    public int hashCode() {
        int result = 17;
        if (getService() != null) {
            if (getService().getServiceName() != null) {
                result = 31 * result + getService().getServiceName().hashCode();
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
            "orderItemId=" + orderItemId +
            '}';
    }
}
