package com.mycompany.WashingApp.entity;

import lombok.NoArgsConstructor;

import jakarta.persistence.*;

@Entity
@NoArgsConstructor

@Table(name = "locations")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_wash_id")
    private Integer locationId;
    @Column(name = "address_name")
    private String addressName;
    @OneToOne
    @JoinColumn(name = "car_wash_id")
    private CarWash carWash;

    public Location(String addressName) {
        this.addressName = addressName;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Override
    public String toString() {
        return "Location{" +
            "locationId=" + locationId +
            ", addressName='" + addressName + '\'' +
            '}';
    }
}
