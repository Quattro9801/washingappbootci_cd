package com.mycompany.WashingApp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;

@Entity
@NoArgsConstructor
@Setter
@Table(name = "reviews")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id")
    private Integer reviewId;
    @Column(name = "content")
    private String content;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE},  fetch = FetchType.LAZY)
    @JoinColumn(name = "car_wash_id")
    private CarWash carWash;

    public Review(Integer reviewId, String content, CarWash carWash) {
        this.reviewId = reviewId;
        this.content = content;
        this.carWash = carWash;
    }

    public Review(String content) {
        this.content = content;
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "Review{" +
            "reviewId=" + reviewId +
            ", content='" + content + '\'' +
            '}';
    }
}
