#Собираем проект
#1) Сборка мавен
FROM maven:3.8.4-openjdk-17 as builder
WORKDIR /app
COPY . /app/.
RUN mvn -f /app/pom.xml clean package -Dmaven.test.skip=true
#2) Используем базовый образ Java
FROM openjdk:17-jdk-slim AS application
#3) Установка редактора nano для просмотра пакетов
RUN apt-get update && apt-get -y install nano
WORKDIR /app
#4) Копируем готовый jar в контейнер
COPY --from=builder /app/target/*.jar /app/WashingAppSnap.jar
#COPY ./logs /app/logs/.
#COPY ./src/main/resources /app/resources/.

#5) Запускаем приложение при старте контейнера
ENTRYPOINT ["java", "-jar", "/app/WashingAppSnap.jar"]

